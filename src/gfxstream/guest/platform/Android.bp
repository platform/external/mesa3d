/*
 * Copyright (C) 2023 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package {
    // See: http://go/android-license-faq
    default_applicable_licenses: ["external_mesa3d_license"],
}

cc_library_static {
    name: "mesa_platform_virtgpu",
    defaults: [
        "mesa_gfxstream_guest_cc_defaults",
    ],
    srcs: [
        "VirtGpu.cpp",
    ],
    header_libs: [
        "mesa_common_headers",
    ],
    static_libs: [
        "mesa_util",
    ],
    shared_libs: [
        "libdrm",
    ],
    export_include_dirs: [
        "include",
    ],
    export_shared_lib_headers: [
        "libdrm",
    ],
    cflags: [
        "-Wno-missing-field-initializers",
        "-fvisibility=default",
        "-fstrict-aliasing",
    ],
    local_include_dirs: [
        "include",
    ],
    target: {
        android: {
            srcs: [
                "linux/LinuxVirtGpuBlob.cpp",
                "linux/LinuxVirtGpuBlobMapping.cpp",
                "linux/LinuxVirtGpuDevice.cpp",
                "linux/LinuxSync.cpp",
                "kumquat/VirtGpuKumquatStub.cpp",
            ],
            shared_libs: [
                "libsync",
            ],
        },
        host: {
            header_libs: [
                "virtgpu_kumquat_ffi_headers",
            ],
            srcs: [
                "linux/LinuxVirtGpuBlob.cpp",
                "linux/LinuxVirtGpuBlobMapping.cpp",
                "linux/LinuxVirtGpuDevice.cpp",
                "linux/LinuxSync.cpp",
                "kumquat/VirtGpuKumquatBlob.cpp",
                "kumquat/VirtGpuKumquatBlobMapping.cpp",
                "kumquat/VirtGpuKumquatDevice.cpp",
                "kumquat/VirtGpuKumquatSync.cpp",
            ],
            shared_libs: [
                "libvirtgpu_kumquat_ffi",
            ],
        },
    },
}

cc_defaults {
    name: "mesa_platform_virtgpu_defaults",
    static_libs: [
        "mesa_platform_virtgpu",
        "mesa_util",
        "mesa_util_c11",
    ],
    target: {
        host: {
            shared_libs: [
                "libvirtgpu_kumquat_ffi",
            ],
        },
    },
}
