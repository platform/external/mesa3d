/*
 * Copyright (C) 2024 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package {
    // See: http://go/android-license-faq
    default_applicable_licenses: ["external_mesa3d_license"],
}

cc_library_static {
    name: "mesa_u_gralloc",
    vendor: true,
    header_libs: [
        "mesa_common_headers",
        "hwvulkan_headers",
        "libnativebase_headers",
    ],
    defaults: [
        "mesa_common_defaults",
    ],
    srcs: [
        "u_gralloc_fallback.c",
        "u_gralloc_cros_api.c",
        "u_gralloc_internal.c",
        "u_gralloc_libdrm.c",
        "u_gralloc_qcom.c",
        "u_gralloc_imapper4_api.cpp",
        "u_gralloc.c",
    ],
    c_std: "c11",
    cpp_std: "c++17",
    static_libs: [
        "libgralloctypes",
        "android.hardware.graphics.mapper@4.0",
    ],
    shared_libs: [
        "libsync",
        "libcutils",
        "libhardware",
        "liblog",
        "libnativewindow",
        "libhidlbase",
        "libutils",
    ],
}
